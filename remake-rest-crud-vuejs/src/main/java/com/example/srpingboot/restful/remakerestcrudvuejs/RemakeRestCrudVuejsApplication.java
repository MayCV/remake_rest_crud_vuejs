package com.example.srpingboot.restful.remakerestcrudvuejs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RemakeRestCrudVuejsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RemakeRestCrudVuejsApplication.class, args);
    }

}
