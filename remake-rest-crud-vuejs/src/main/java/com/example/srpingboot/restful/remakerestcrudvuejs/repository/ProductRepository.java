package com.example.srpingboot.restful.remakerestcrudvuejs.repository;

import com.example.srpingboot.restful.remakerestcrudvuejs.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
